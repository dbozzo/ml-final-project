# Matthew J. Malir
# ML Final Project
# Wildfire EDA
# 23 Feb 2020

# import necessary packages
library(dplyr)
library(wesanderson)

# set wd
setwd("/Users/MJMalir/Desktop/ML/final_proj")

# import table
data <- read.csv("data/data.csv")

### Initial Filtering
# remove rows w/ irrelevant labels
fires <- data %>% 
  filter(!is.na(STAT_CAUSE_DESCR)) %>% 
  filter(!(STAT_CAUSE_DESCR %in% c("Missing/Undefined","Miscellaneous"))) %>% 
  droplevels.data.frame()
  
### EDA

# check frequency of labels
table(fires$STAT_CAUSE_DESCR)

# frequency plot
par(las=2) # make label text perpendicular to axis
par(mar=c(7,4,1,0)) # increase y-axis margin.
barplot(table(fires$STAT_CAUSE_DESCR) / 1000, ylab = "Count (Thousands)",  col=topo.colors(11))

# year by year fire totals
mean(table(fires$FIRE_YEAR))
