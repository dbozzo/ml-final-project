# Wildfire ML Project
# purpose: remove CSV file prior to Git push

## imports
import os

## change dir to data/
os.chdir('./data')

## remove csv file if necessary
if os.path.exists('data.csv'):
  os.remove('data.csv')
