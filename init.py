# Wildfire ML Project
# purpose: unzip data file
# source: https://thispointer.com/python-how-to-unzip-a-file-extract-single-multiple-or-all-files-from-a-zip-archive/

## imports
from zipfile import ZipFile
import os

## change dir to data/
os.chdir('./data')

## extract data file if necessary
if not os.path.exists('data.csv'):
  with ZipFile('data.zip', 'r') as zipObj:
    # extract contents of zip into current dir
    zipObj.extractall()

## remove unnecessarily generated folder
if os.path.exists('__MACOSX/'):
  os.rmdir('__MACOSX/')
