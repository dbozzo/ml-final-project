# Wildfire (ML Final Project)
### Dom J. Bozzo, Matthew J. Malir, Connor J. Bach, Allen J. Duong
### 25 April 2020
------------------------------------------------------------------

## Running our project
1. run "python init.py" to unzip data file
2. move into the **code/** folder
3. run "wildfire.ipynb" in jupyter notebook
4. return to parent directory
5. find exported visuals & metrics in **output/** folder
6. run "python clean.py" to remove csv file